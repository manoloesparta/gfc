import binascii

ops = {
    'add': '0',
    'sub': '1',
    'and': '2',
    'or': '3',
}

def convert(decimal):
    hexa = {10:'A',11:'B', 12:'C', 13:'D', 14:'E', 15:'F'}
    result = []

    if decimal == 0:
        return '0'
    elif decimal < 0:
        decimal = (~decimal) + 1

    while decimal != 0:
        tmp = decimal % 16
        if tmp >= 10:
            tmp = hexa[tmp]
        result.insert(0, str(tmp))
        decimal = decimal // 16

    return ''.join(result)

def save():
    with open('fok.asm') as f:

        line = f.readline()
        content = ' '
        result = ''

        while line and content[0] != '':
            content = line.lower().rstrip().split(' ')

            instruction, number = ops[content[0]], content[1]

            result += f'{instruction}{convert(int(number))}'

            line = f.readline()

    with open('fok.hex', 'wb') as f:
        f.write(binascii.unhexlify(result))

if __name__ == '__main__':
    save()
