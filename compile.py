from compiler import Lexer, Compiler

def interpreter():
    while True:
        entrada = input('fuck> ')
        l = Lexer(entrada)
        c = Compiler(l)
        r = c.expr()
        print(r)

def optimized():
    with open('input.fok', 'r') as file, open('fok.asm', 'w') as out:
        content = file.readline()


        while content:
            l = Lexer(content)
            c = Compiler(l)
            r = c.expr()

            if r > 15:
                raise Exception('Operations with more than 4 bits not allowed')

            content = file.readline()

            out.write('and 0\n')
            out.write(f'add {r % 16}\n')

if __name__ == "__main__":
    optimized()