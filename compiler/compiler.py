from .constant import *
from .lexer import Lexer

class Compiler(object):
    def __init__(self, lexer):
        self.lexer = lexer
        self.current_token = self.lexer.get_next_token()

    def error(self):
        raise Exception('syntax error')

    def eat(self, token_type):
        if self.current_token.type == token_type:
            self.current_token = self.lexer.get_next_token()
        else:
            self.error()

    def factor(self, show=True):
        token = self.current_token
        self.eat(INTEGER)
        return token.value

    def expr(self):
        result = self.factor()

        while self.current_token.type in (MUL, PLUS, MINUS, AND, OR, END):
            token = self.current_token

            if token.type == END:
                self.eat(END)

            if token.type == MUL:
                self.eat(MUL)
                tmp = self.factor(False)
                result = result * tmp

            elif token.type == PLUS:
                self.eat(PLUS)
                result = result + self.factor()
                
            elif token.type == MINUS:
                self.eat(MINUS)
                result = result - self.factor()
                
            elif token.type == AND:
                self.eat(AND)
                result = result - self.factor()
                
            elif token.type == OR:
                self.eat(OR)
                result = result - self.factor()

        return result