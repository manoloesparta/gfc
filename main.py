from os import system
from assembler import save
from compile import optimized

def main():
    optimized()
    save()

if __name__ == "__main__":
    main()